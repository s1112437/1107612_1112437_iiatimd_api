<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderSchemas extends Model
{
    protected $table="reminder_schema";

    public $timestamps = false;

    public function Reminder(){
        return $this->belongsTo("App\Reminders");
    }
}
