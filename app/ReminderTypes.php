<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderTypes extends Model
{
    protected $table="reminder_types";

    public $timestamps = false;
}
