<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminders extends Model
{
    protected $table="reminders";

    public $timestamps = false;

    public function ReminderSchema(){
        return $this->hasOne("App\ReminderSchemas", 'onlineReminderId');
    }

    public function Users(){
        return $this->belongsTo("App\User");
    }
}
