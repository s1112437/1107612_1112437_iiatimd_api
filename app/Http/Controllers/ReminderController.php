<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reminders;
use App\ReminderSchemas;
use Illuminate\Support\Facades\Auth;
class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Reminders::where("users_id","=", Auth::user()->id)->with("ReminderSchema")->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reminder = new Reminders;
        $reminder->reminderTitle = $request->reminderTitle;
        $reminder->reminderTypeId = $request->reminderTypeId;
        $reminder->localId = $request->localId;
        $reminder->opmerkingen = $request->opmerkingen;
        $reminder->Aantal = $request->Aantal;
        $reminder->Dosis = $request->Dosis;
        $reminder->alarmId = $request->alarmId;
        $reminder->imageId = $request->imageId;
        $reminder->extraInfo = $request->extraInfo;
        $reminder->users_id = Auth::user()->id;
        $reminder->save();

        return $reminder;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reminder = Reminders::find($id);
        $reminderSchema = $reminder->reminderSchema()->first();
        $reminderSchema = ReminderSchemas::find($reminderSchema->id);
        $reminderSchema->delete();
        $reminder->delete();

        return response()->json([
            "msg" => "Reminder succesvol verwijderd"
        ]);
    }
}
