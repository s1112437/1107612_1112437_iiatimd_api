<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Images;
use Intervention\Image\ImageManagerStatic as Image;
use App\ReminderTypes;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Images::all();
        $output = array();
        foreach ($images as $image){
            $outputImage = [
                "id" => $image->id,
                "image" => base64_encode($image->image),
                "reminderTypeId" => $image->reminderTypeId
            ];
            array_push($output, $outputImage);
        }
        return $output;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image')->store('public/images');
        $file = file_get_contents(asset('storage/images/' . str_replace('public/images/','',$image)));
        $image = Image::make($file)->encode();
        Images::insert([
            'image' => $image,
            'reminderTypeId' => $request->reminderTypeId
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = Images::find($id);
        return response()->json(["id" => $image->id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function create(){
        $reminderTypes = ReminderTypes::all();
        return view('createImage')->with('reminderTypes', $reminderTypes);
    }
}
