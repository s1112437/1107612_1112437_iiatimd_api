<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReminderSchemas;

class ReminderSchemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ReminderSchemas::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reminderSchema = new ReminderSchemas();
        $reminderSchema->onlineReminderId = $request->onlineReminderId;
        $reminderSchema->localReminderId = $request->localReminderId;
        $reminderSchema->localId = $request->localId;
        $reminderSchema->repeat = $request->repeat;
        $reminderSchema->repeatFrequency = $request->repeatFrequency;
        $reminderSchema->date = date("Y-m-d", substr($request->date, 0, 10));
        $reminderSchema->heleDag = $request->heleDag;
        $reminderSchema->startTijd = date("H:i:s", $request->startTijd/1000);
        $reminderSchema->eindTijd =  date("H:i:s", $request->eindTijd/1000);

        $reminderSchema->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ReminderSchema::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
