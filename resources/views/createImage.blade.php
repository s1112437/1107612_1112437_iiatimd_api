<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="/api/image" method="post" enctype="multipart/form-data">
        <input type="file" name="image" id="image">
        <select name="reminderTypeId" id="reminderTypeId">
            @foreach ($reminderTypes as $reminderType)
            <option value="{{$reminderType->id}}">{{$reminderType->reminderTypeTitle}}</option>
            @endforeach
        </select>
        <button type="submit">Opslaan</button>
    </form>
</body>
</html>