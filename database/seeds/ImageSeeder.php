<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = file_get_contents(asset('storage/images/contactlens.jpg'));
        $image = Image::make($file)->encode();
        DB::table("image")->insert([
            'image' => $image,
            'reminderTypeId' => 1
        ]);
    }
}
