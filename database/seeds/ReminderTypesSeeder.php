<?php

use Illuminate\Database\Seeder;

class ReminderTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("reminder_types")->insert([
            'reminderTypeTitle' => "Medische Herinnering"
        ]);
    }
}
