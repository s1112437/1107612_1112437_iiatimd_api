<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReminderSchemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminder_schema', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("onlineReminderId");
            $table->integer("localId");
            $table->unsignedBigInteger("localReminderId");
            $table->boolean("repeat");
            $table->string("repeatFrequency")->nullable();
            $table->date("date");
            $table->boolean("heleDag");
            $table->time("startTijd");
            $table->time("eindTijd");

            $table->foreign("onlineReminderId")->references("id")->on("reminders");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reminder_schema', function (Blueprint $table) {
            $table->dropForeign('reminder_schema_onlinereminderid_foreign');
        });
        Schema::dropIfExists('reminder_schema');
    }
}
