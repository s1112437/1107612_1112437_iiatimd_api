<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminders', function (Blueprint $table) {
            $table->id();
            $table->string("reminderTitle");
            $table->unsignedBigInteger("reminderTypeId");
            $table->integer("localId");
            $table->String('opmerkingen');
            $table->integer("Aantal");
            $table->integer("Dosis");
            $table->integer("alarmId");
            $table->unsignedBigInteger("imageId");
            $table->string("extraInfo");
            $table->unsignedBigInteger("users_id");

            $table->foreign("users_id")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reminders', function (Blueprint $table) {
            $table->dropForeign('reminders_users_id_foreign');
        });
        Schema::dropIfExists('reminders');
    }
}
