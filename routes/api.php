<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Api\Auth\RegisterController@register');
Route::post('login', 'Api\Auth\LoginController@login');
Route::post('refresh', 'Api\Auth\LoginController@refresh');


Route::middleware('auth:api')->group(function () {
    Route::post('logout', 'Api\Auth\LoginController@logout');
    Route::get('/reminders', 'ReminderController@index');
    Route::post('/reminder', 'ReminderController@store');
    Route::delete("/reminder/{id}", 'ReminderController@destroy');
    Route::get('/images', 'ImagesController@index');
    Route::get('/reminderSchemas', 'ReminderSchemaController@index');
    Route::post("/reminderSchema", 'ReminderSchemaController@store');
    Route::get('/reminderTypes', 'ReminderTypeController@index');
    Route::get('/reminder/{id}', 'ReminderController@show');
    Route::get('/image/{id}', 'ImagesController@show');
    Route::get('/user', 'UserController@show');
});

Route::post('/image', 'ImagesController@store');

